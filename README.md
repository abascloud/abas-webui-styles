## &lt;abas-styles&gt;

The `<abas-styles>` component provides simple ways to use Material Design CSS styles
in your application. The following imports are available:

1. abas-color.html:
A complete list of the abas colors

2. abas-default-theme.html: Text,
background and accent colors that match the default abas Design theme

3. abas-shadow.html: 
Elevation and shadow styles

4. abas-typography.html:
Font styles and sizes


We recommend importing each of these individual files, and using the style mixins
available in each ones, rather than the aggregated `abas-styles.html` as a whole.
